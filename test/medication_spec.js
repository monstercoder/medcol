var util = require('./utils'),
supertest = require("supertest"),
app=require("../server"),
api= supertest(app),
expect = require("chai").expect,
should = require("chai").should(),
Patient = require('../api/models/patient')

var Bpmh = require('../api/models/bmph');
var patient, bpmh;

var medications = []
for(var i = 0; i <5 ; i++) {
    medications.push({
        name:"medication_"+i,
        dose: "dose"+i,
        route: "route_"+i,
        frequency: "freq"+i,
        last_dose: "last_dose"+1,
        action: ["continue","discontinue","change"][i % 3],
        change_to: {
            name: "change_to_medication_"+i,
            dose: "change_to_dose_"+i,
            route: "change_to_route_"+i,
            frequency: "change_to_frequency"+i,
            reason: "reason for change"+i
        }})
bpmh
}

describe("patient medication record", function() {
    beforeEach(function(done) {
        util
        .find_a_patient()
        .then(
            function(p){
                patient = p;
                Bpmh.findOne({patient_id: p._id}, function(err, b) {
                    bpmh = b;
                    bpmh.medications = medications
                    bpmh.save(function(err, b){
                        if (err){
                              console.log("%%%", err) 
                          }
                          done();

                    })
                })
            }
        )

    });
    
    it ("updates medication changed", function(done){
        var m = bpmh.medications[0];
        m.action = "change"
        m.change_to = {
            name: 'aaaaaa',
            dose: '777',
            route: 'ooo',
            frequency: '222',
            reason: 'ddd'
        }

        
        api
        .post('/api/patient/'+bpmh._id+"/medication/"+m._id)
        .send({data:m})
        .end(function(err, res) {
            expect(res.statusCode).to.be.equal(200);
            expect(res.body.action).to.be.equal('change');
            //expect(res.body.change_to).to.deep.equal(m.change_to);
            
            expect(res.body.change_to.name).to.equal(m.change_to.name);
            expect(res.body.change_to.dose).to.equal(m.change_to.dose);
            expect(res.body.change_to.route).to.equal(m.change_to.route);
            expect(res.body.change_to.frequency).to.equal(m.change_to.frequency);
            expect(res.body.change_to.reason).to.equal(m.change_to.reason);
            
            done();
        })
       
    })
})