var utils = require('./utils'),
supertest = require("supertest"),
app=require("../server"),
api= supertest(app),
expect = require("chai").expect,
should = require("chai").should(),
Patient = require('../api/models/patient')

describe("Patient", function() {
    describe("CRUD", function() {
        it("creates a patient", function(done) {
            api.post("/api/patients")
            .send({
                patient_number : "#1",
                firstname : "test",
                middlename: "",
                lastname  : "user",
                gender    : "F",
                birthday  : Date()
            })
            .end(function(err,res) {
                expect(res.statusCode).to.equal(200);
                expect(res.body.success).to.equal(true);
                done();
            })
        })
        
        it("update a patient", function(done) {
            var a_day =  Date();
            var expected = {
                phin: "123456",
                patient_number : "#2",
                firstname : "test",
                middlename: "",
                lastname  : "user",
                gender    : "F",
                birthday  : a_day,
                note: "some note",
                address:  {
                            line_1: "55 Five St.",
                            line_2: "",
                            city: "Winnipeg",
                            province: "MB",
                            postal: "r3y1r4"
                }
            };
            Patient.findOne({}, function(err, p) {
                api.post("/api/patient/"+p._id)
                 .send(expected)
                 .end(function(err,res) {
                     var actual = res.body;
                       delete actual._id
                       delete actual.last_updated
                       delete actual.birthday
                       delete actual.__v
                       delete expected.birthday

                     expect(res.statusCode).to.equal(200);
                     expect(actual).to.be.deep.equal(expected);
                     done();
                 })
            })  

            
 
        })
        
        
        it("lists all patients", function(done) {
            
            api.get('/api/patients')
            .end(function(err, res) {
                expect(res.statusCode).to.equal(200);
                expect(res.body.length).to.equal(9);
                done();
            })
        })
        
        it ("returns a patient", function(done) {
            Patient.findOne({}, function(err, p) {
                api.get('/api/patient/'+p.id)
                .end(function(err,res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body._id).to.equal(p.id);
                    expect(res.body.firstname).to.equal(p.firstname);
                    done();
                })
            })
        })
    })
})
