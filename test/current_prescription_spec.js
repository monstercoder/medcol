var util = require('./utils'),
supertest = require("supertest"),
app=require("../server"),
api= supertest(app),
expect = require("chai").expect,
should = require("chai").should(),
Patient = require('../api/models/patient')
Bpmh = require('../api/models/bmph')
PhysicianOrders = require('../api/models/physicianOrders')

var medications = []
for(var i = 0; i <5 ; i++) {
    medications.push({
        name:"medication_"+i,
        dose: "dose"+i,
        route: "route_"+i,
        frequency: "freq"+i,
        last_dose: "last_dose"+1,
        action: ["continue","discontinue","change"][i % 3],
        reconciliation: {
                   "resolved": false
            
        },
        change_to: {
            name: "change_to_medication_"+i,
            dose: "change_to_dose_"+i,
            route: "change_to_route_"+i,
            frequency: "change_to_frequency"+i,
            reason: "reason for change"+i
        }})
}

var medications_update = []
for(var i = 8; i <10 ; i++) {
    medications_update.push({
        name:"medication_"+i,
        dose: "dose"+i,
        route: "route_"+i,
        frequency: "freq"+i,
        last_dose: "last_dose"+1,
        action: ["continue","discontinue","change"][i % 3],
        change_to: {
            name: "change_to_medication_"+i,
            dose: "change_to_dose_"+i,
            route: "change_to_route_"+i,
            frequency: "change_to_frequency"+i,
            reason: "reason for change"+i
        }})
}
describe("Current Prescription", function () {
    
    xit("new patient has current prescription of bpmh", function(done) {
            util
            .find_a_patient()
            .then(
                function(p) {
                    var allergy = {
                        hasAllergy: "Yes",
                        content: "a, b, c"
                    };
                    Bpmh.findOne({patient_id: p._id}, function(err, b) {
                        bmph = b;
                        bmph.allergy = allergy;
                        bmph.medications = medications;
                        bmph.save(function(err, b){
                            if (err){
                                  console.log("%%%", err) 
                            }
                            
                            
                            api.get('/api/patient/'+p._id+'/current_prescription')
                                .end(function(err,res) {
                                   // console.log("%%%", err, res.body) 
                                    expect(res.statusCode).to.equal(200);
                                    expect(removeIdProperty(res.body.reconciled_medications)).to.be.deep.equal(medications);
                                    expect(res.body.bpmh_id == bmph._id).to.be.true
                                    expect(res.body.allergy).to.be.deep.equal(allergy);
                                    done();
                            })

                        })
                    })


                }   
            );
        }
    );
    
    it(" returns current prescription", function(done) {
        var order;
        util
        .find_a_patient()
        .then(function(p) {
                    order = new PhysicianOrders({
                        medications: medications,
                        prescriber : { 
                            name: "aaa",
                            number: "123456"
                        }
                    }
                    );
                    
                    order.patient_id =p._id;
                    
                    return order.save()
                }
            )
        .then(function(o) {
                 return api.get('/api/patient/'+o.patient_id+'/current_prescription')   
            })
        .then(function(get_result) {
                get_result
                .end(function(err,  res) {
                      //console.log("********",  res.body.physician_orders)
                      expect(res.statusCode).to.equal(200);
                      expect(removeIdProperty(res.body.physician_orders[0].medications)).to.be.deep.equal(medications);
                      expect(res.body.physician_orders[0].prescriber).to.be.include({ 
                          name: "aaa",
                          number: "123456"
                      });
                      done();
                  })
            })
        })
        
    describe("physicion order", function() {
        it (" creates a physician order", function(done) { 
             var order;
            util
            .find_a_patient()
            .then(function(p) {
                order = {
                      patient_id: p._id,
                      medications: medications,
                      prescriber : { 
                          name: "bbb",
                          number: "23456"
                      }
                };

                return api.post('/api/patient/'+p._id+'/physicion_order')
                    .send(order)
            })
            .then(function(post_result) {
                post_result.end(function(err, res) {
                     expect(res.statusCode).to.equal(200);
                     //console.log(res.body,prescriber, order.prescriber);
                     expect(res.body.prescriber).to.include(order.prescriber);
                     expect(removeIdProperty(res.body.medications)).to.be.deep.equal(order.medications);
                     done();
                })
            })

        });

        xit(" updates an physician order", function(done) {
            var order;
            util
            .find_a_patient()
            .then(function(p) {
                        order = new PhysicianOrders({
                            medications: medications,
                            prescriber : { 
                                name: "aaa",
                                number: "123456"
                            }
                        }
                        );

                        order.patient_id =p._id;

                        return order.save()
                    }
                )
            .then(function(o) {                
                    return api.put('/api/patient/'+o.patient_id+'/physicion_order/'+o._id)
                    .send({
                        medications: medications_update,
                        prescriber : { 
                            name: "ccc",
                            number: "789012"
                        }
                    })   
            })
            .then(function(post_result) {
                 post_result.end(function(err, res) {
                      expect(res.statusCode).to.equal(200);
                      expect(res.body.patient_id == order.patient_id).to.be.true;
                      //console.log(res.body,prescriber, order.prescriber);
                      expect(res.body.prescriber).to.include({ 
                          name: "aaa",
                          number: "123456"
                      });
                      expect(removeIdProperty(res.body.medications)).to.be.deep.equal(medications_update);
                      done();
                 })
             })
        })

        it(" remove a physician order", function(done) {
            var order;
            util
            .find_a_patient()
            .then(function(p) {
                        order = new PhysicianOrders({
                            medications: medications,
                            prescriber : { 
                                name: "aaa",
                                number: "123456"
                            }
                        }
                        );

                        order.patient_id =p._id;

                        return order.save()
                    }
                )
            .then(function(o) {
                     return api.delete('/api/patient/'+o.patient_id+'/physicion_order/'+ o._id)   
            })
            .then(function(api_result) {
                 api_result.end(function(err, res) {
                      expect(res.statusCode).to.equal(200);

                      PhysicianOrders.findById(order._id, function(err, o) {
                          // console.log("&&&&", err, o)
                          expect(o).to.be.null;
                          done();
                      })

                 })
             })
        })
    })
        
    


})

function removeIdProperty(obj) {
    if (Object.prototype.toString.call(obj) === '[object Array]') {
        var out = [], i = 0, len = obj.length;
        for ( ; i < len; i++ ) {
            var item = arguments.callee(obj[i]);
            delete item._id
            out[i] = item
        }
        return out;
    }
    if (typeof obj === 'object') {
        var out = {}, i;
        for ( i in obj ) {
            out[i] = arguments.callee(obj[i]);
        }
        delete out._id
        return out;
    }
    delete obj._id
    return obj;
}