var mongoose = require('mongoose');
var config = require("../api/config/config");
var Patient = require('../api/models/patient');

// ensure the NODE_ENV is set to 'test'
// this is helpful when you would like to change behavior when testing
process.env.NODE_ENV = 'test';

beforeEach(function (done) {
 function clearDB() {
   for (var i in mongoose.connection.collections) {
     mongoose.connection.collections[i].remove(function() {});
   }
 }

 if (mongoose.connection.readyState === 0) {
   mongoose.connect(config.database, function (err) {
     if (err) {
       throw err;
     }
      clearDB();
      createPatients(9, done)
   });
 } else {
    clearDB();
    createPatients(9, done)
 }
}); 

var patients = []
function createPatients(count,done) {
    for (var i=0;i <count; i++) {
        var patient = new Patient({
            patient_number : "firstname"+i,
            firstname : i,
            middlename: "",
            lastname  : "lastname"+i,
            gender    : "F",
            birthday  : Date()
        })

        patient.save(function(err,u) {
             if (err) {
                 return console.log("--- before ---","failed", err);
             } else {
                 patients.push(u);
                 // return console.log(" --- before ---","created user", u);
             }
             
             if (i = count-1) {
                 done();
             }
         });
    };
}

module.exports= {
    createPatients: createPatients,
    patients: patients,
    find_a_patient: function() {
        return Patient
        .findOne()
        .exec()
    },    
    removeIdProperty: function (obj) {
                for (var i =0 ; i < obj.length; i++ ) {
                    var item = arguments.callee(obj[i]);
                    item._id = undefined
                    item._v = undefined
                    

                }
                return obj;
   
    }
}

