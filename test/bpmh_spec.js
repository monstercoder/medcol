var utils = require('./utils'),
supertest = require("supertest"),
app=require("../server"),
api= supertest(app),
expect = require("chai").expect,
should = require("chai").should(),
Patient = require('../api/models/patient')

var Bmph = require('../api/models/bmph');
var patient, bmph;

var medications = []
for(var i = 0; i <5 ; i++) {
    medications.push({
        name:"medication_"+i,
        dose: "dose"+i,
        route: "route_"+i,
        frequency: "freq"+i,
        last_dose: "last_dose"+1,
        action: ["continue","discontinue","change"][i % 3],
        history: {
                   "admission_prescription": false,
                   "changes": []
        },
        reconciliation: {
                   "resolved": false
            
        },
        change_to: {
            name: "change_to_medication_"+i,
            dose: "change_to_dose_"+i,
            route: "change_to_route_"+i,
            frequency: "change_to_frequency"+i,
            reason: "reason for change"+i
        }})

}

var medications_update = []
for(var i = 10; i <12 ; i++) {
    medications_update.push({
        name:"medication_"+i,
        dose: "dose"+i,
        route: "route_"+i,
        frequency: "freq"+i,
        last_dose: "last_dose"+1,
        action: ["continue","discontinue","change"][i % 3],
        change_to: {
            name: "change_to_medication_"+i,
            dose: "change_to_dose_"+i,
            route: "change_to_route_"+i,
            frequency: "change_to_frequency"+i,
            reason: "reason for change"+i
        },
        history: {
                   "admission_prescription": true,
                   "changes": []
        },
        reconciliation: {
                   "resolved": false
            
        }
    })
}

var invalid_medication_input = [{
            name: "medication_invalide",
            action: '',
            last_dose: 'dose',
            type: ["prescription","non-prescription","alternative"][2]
    }]

describe("Bpmh", function () {
    
    it("new patient with default bpmh", function(done) {
            Patient
            .findOne()
            .exec()
            .then(
                function(p) {
                    api.get('/api/patient/'+p._id+'/bpmh')
                        .end(function(err,res) {
                            expect(res.statusCode).to.equal(200);
                            expect(res.body.patient_id==p._id).to.be.true
                            expect(removeIdProperty(res.body.medications)).to.be.deep.equal([{"action": "continue", 
                                "history": {
                                       "admission_prescription": false,
                                       "changes": []
                                },
                                "reconciliation": {
                                       "resolved": false
                                }
                            }])
                            expect(res.body.allergy.hasAllergy=='No').to.be.true;
                            done();
                        })
                }   
            );
        }
    );
    
    it("new patient creates bpmh", function(done) {
            bmph = new Bmph({
                        medications: medications,
                        allergy: []
            });
            Patient
            .findOne()
            .exec()
            .then(
                function(p){
                    bmph.patient_id = p._id;
                    bmph.medications = []
                    api.post('/api/patient/'+p._id+'/bpmh')
                    .send(bmph)
                    .end(function(err,res) {
                        Bmph.find({patient_id: p._id}, function(err, found) {
                              expect(found.length).to.be.equal(1);
                              done()
                        })


                    })
                }
            );
        }
    );
    
    describe("exiting bpmh", function() {          
        beforeEach(function(done) {
            Patient
            .findOne()
            .exec()
            .then(
                function(p){
                    patient = p;
                    Bmph.findOne({patient_id: p._id}, function(err, b) {
                        bmph = b;
                        bmph.medications = medications
                        bmph.save(function(err, b){
                            if (err){
                                  console.log("%%%", err) 
                              }
                              done();

                        })
                    })
                }
            )

        });
        
        it("returns bpmh for patient", function(done) {

            api.get('/api/patient/'+patient._id+'/bpmh')
                     .end(function(err,res) {
                         expect(res.statusCode).to.equal(200);
                         expect(removeIdProperty(res.body.medications)).to.be.deep.equal(medications);
                         expect(res.body.allergy.hasAllergy).to.be.eql(bmph.allergy.hasAllergy)
                         done();
            })

        });
        

        
        it("updates bpmh for patient", function(done) {
            bmph.medications = medications_update
            api.post('/api/patient/'+patient._id+'/bpmh')
            .send(bmph)
            .end(function(err,res) {
                expect(res.statusCode).to.equal(200);
                expect(removeIdProperty(res.body.bmph.medications)).to.be.deep.equal(medications_update);
                expect(res.body.bmph.allergy.hasAllergy).to.be.eql(bmph.allergy.hasAllergy)
                Bmph.find({patient_id: patient._id}, function(err, found) {
                      expect(found.length).to.be.equal(1);
                      done()
                })
               
                
            })
        })
        
        it ( 'rolls back when updating medications faild', function(done) {
            bmph.medications = invalid_medication_input
            api.post('/api/patient/'+patient._id+'/bpmh')
            .send(bmph)
            .end(function(err,res) {
                expect(res.statusCode).to.equal(500);
                expect(res.body.success).to.be.false
    
                Bmph.findOne({patient_id: patient._id}, function(err, found) {
                   //console.log('###', err, found.medications.length, medications.length)
                    expect(found.medications.length).to.equal(medications.length);
                    done()
               })
                
            })
        })
        
        it ( 'rolls back when updating allergy faild', function(done) {
            bmph.allergy = {
                hasAllergy: "invalid",
                content: "String"
            };
            
            api.post('/api/patient/'+patient._id+'/bpmh')
            .send(bmph)
            .end(function(err,res) {
                expect(res.statusCode).to.equal(500);
                expect(res.body.success).to.be.false
    
                Bmph.findOne({patient_id: patient._id}, function(err, found) {
                    expect(found.allergy.hasAllergy).to.equal('No');
                    done()
               })
                
            })
        })
    })
})

function removeIdProperty(obj) {
    if (Object.prototype.toString.call(obj) === '[object Array]') {
        var out = [], i = 0, len = obj.length;
        for ( ; i < len; i++ ) {
            var item = arguments.callee(obj[i]);
            delete item._id
            out[i] = item
        }
        return out;
    }
    if (typeof obj === 'object') {
        var out = {}, i;
        for ( i in obj ) {
            out[i] = arguments.callee(obj[i]);
        }
        delete out._id
        return out;
    }
    delete obj._id
    return obj;
}

