var utils = require('./utils'),
config= require('../api/config/config')
supertest = require("supertest"),
app=require("../server"),
api= supertest(app),
expect = require("chai").expect,
should = require("chai").should(),
User = require('../api/models/user'),
webtoken = require('jsonwebtoken');
function decodeToken(token) {
    
    return webtoken.verify(token, config.secretKey)
};
 
  
describe("#login", function() {
    
    beforeEach(function(done) {
        user = new User({
             name: "test user",
             username: "test",
             password: "123456"
         });

         user.save(function(err,u) {
             if (err) {
                 return console.log("--- before ---","failed", err);
             } 
         });


         return done();
    });
    describe("authentication", function() {
        it("user not exist then returns 403 unauthorize error", function(done) {
            api
            .post('/api/login')
            .send({ username: "not_exist", password: ""})
            .expect(403, done);
        })

        it("returns 403 when username password not match", function(done) {
            api
            .post('/api/login')
            .send({ username: "test", password: "789"})
            .expect(403, done);
        })

        it("returns token when success", function(done) {
            api
            .post('/api/login')
            .send({ username: "test", password: "123456"})
            .expect(200, done);
        })
    })
    
    describe("authorization", function() {
        it(" returns default permission if not specified", function(done) {
            //this.slow(100000)
            api.post('/api/login')
            .send({ username: "test", password: "123456"})
            .end(function(err, res) {
                var decoded = decodeToken(res.body.token);
                expect(decoded.username).to.eql(user.username);
                //expect(decoded).to.be.have.deep.property('role[0].name', 'clerk');
                //console.log('***', res.body.roles)
                //expect(res.body).to.be.have.deep.property('roles[0].name', 'clerk');
                done();
            })
        })
    })

}) 

      
