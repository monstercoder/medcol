var config = require('./api/config/config')
var morgan = require('morgan');
var express = require('express');
var mongoose = require('mongoose')
var bodyParser = require('body-parser');

mongoose.connect(config.database, function(err) {
    if (err) {
        console.log(err);
    } else {
        console.log("Database connected!",config.database)
    }
})

var app = express();
var api = require('./api/routes/api')(express, app);
var patientApi = require('./api/routes/patient')(express, app);
var physicianOrderApi = require('./api/routes/physician_order')(express, app);
var medicationApi = require('./api/routes/medication')(express, app);
var reconciliationApi = require('./api/routes/reconciliation')(express, app);
var current_prescription = require('./api/routes/current_prescription')(express, app);
var transferApi = require('./api/routes/transfer')(express, app);
var dischargeApi = require('./api/routes/discharge')(express, app);
var userApi = require('./api/routes/user')(express, app);
app.use(bodyParser.urlencoded({extended: true}));

app.use(bodyParser.json())

if (process.env.NODE_ENV != 'test') {
  app.use(morgan('combined'));  
}

app.use(express.static('public'));

app.use('/api', api);
app.use('/api', userApi);
app.use('/api', patientApi);
app.use('/api/patient', physicianOrderApi);
app.use('/api/patient', medicationApi);
app.use('/api/patient', reconciliationApi);
app.use('/api/patient', current_prescription)
app.use('/api/patient', transferApi)
app.use('/api/patient', dischargeApi)
app.get('*', function(req, res) {
    res.sendFile(__dirname+"/public/app/views/index.html")
});


app.listen(config.port, function(err) {
    if (err) {
        console.log(err)
    } else {
        console.log("Listening port " + config.port);
    }
})


module.exports= app
