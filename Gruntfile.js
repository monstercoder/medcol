module.exports = function(grunt){
    grunt.loadNpmTasks('grunt-env');
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.initConfig({
      // Configure a mochaTest task
      env : {
        options : {
        //Shared Options Hash 
        },
        dev : {
          NODE_ENV : 'development',
          DEST     : 'temp'
        },
        test : {
          NODE_ENV : 'test',
        }
      },
      watch: {
        scripts: {
          files: ['api/**/*.js', 'test/**/*.js'],
          tasks: ['test'],
          options: {
            spawn: false,
          },
        },
      },
      mochaTest: {
        test: {
          options: {
            clearRequireCache: true,
            reporter: 'spec'
          },
          src: ['test/**/*_spec.js']
        }
      }
    });
  
  grunt.registerTask('test', ['env:test', 'mochaTest']);

  grunt.registerTask('default', ['test']);
};