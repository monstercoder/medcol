module.exports = {
    port: process.env.PORT || 3000,
    database: function() {
        if (process.env.NODE_ENV == 'test' ) {
            return "mongodb://127.0.0.1:27017/medcol_test"
        }
        
        return "mongodb://127.0.0.1:27017/medcol"
    }(),
    secretKey: process.env.SECRETKEY || "save the summer"
}