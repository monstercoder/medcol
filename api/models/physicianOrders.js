var mongoose = require('mongoose');
var medicationSchema= require('./medication');

var physicianOrdersSchema = new mongoose.Schema({
    patient_id: { type: mongoose.Schema.ObjectId, ref:'Patient', required: true},
    medications: [
        new mongoose.Schema({
                name: {type: String, trim: true},
                dose: String,
                route: {type: String, trim: true},
                frequency: {type: String, trim: true},
                last_dose: String,
                action: {type: String, default: 'continue', required: true, enum:["continue","discontinue","change"]},
                change_to: {
                    name: {type: String, trim: true},
                    dose: String,
                    route: {type: String, trim: true},
                    frequency: {type: String, trim: true},
                    reason: String
                },
                reconciliation: {
                        reason: { type: String, enum:["no_discrepancy", "undocumented","unintended"] },
                        resolved: { type: Boolean, default: false }
                }
        })
    ],
    prescriber : { 
        name: String,
        number: String,
        time: { type: Date, default: Date.now }
    },
    last_updated: {
        by: String,
        time: Date
    }
});

module.exports = mongoose.model("PhysicianOrders", physicianOrdersSchema);