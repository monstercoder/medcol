var mongoose = require('mongoose');
var medicationSchema= require('./medication');

var dischargePlanSchema = new mongoose.Schema({
    patient_id: { type: String, trim: true },
    created_time: { type: Date, default: Date.now() },
    medications: [{}],
    general_orders: [{}],
    discontinued: [{}],
    allergies: String
});

module.exports = mongoose.model("DischargePlan", dischargePlanSchema);