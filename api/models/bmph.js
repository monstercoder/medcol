var mongoose = require("mongoose");
var findOrCreate = require('mongoose-findorcreate')

var bmphModel = new mongoose.Schema({
    patient_id: { type: mongoose.Schema.ObjectId, ref:'Patient', required: true},
    completed: {type: Boolean, default: false},
    allergy: {
        hasAllergy: {type: String, default: "No", enum:['Yes','No','Unknow'] },
        content: String
    },
    medications: [
        new mongoose.Schema({
                name: {type: String, trim: true},
                dose: String,
                route: {type: String, trim: true},
                frequency: {type: String, trim: true},
                last_dose: String,
                action: {type: String, default: 'continue', required: true, enum:["continue","discontinue","change"]},
                change_to: {
                    name: {type: String, trim: true},
                    dose: String,
                    route: {type: String, trim: true},
                    frequency: {type: String, trim: true},
                    reason: String
                },
                reconciliation: {
                    reason: { type: String, enum:["no_discrepancy", "undocumented","unintended"] },
                    resolved: { type: Boolean, default: false }
                },
                history: {
                    admission_prescription: {
                        type: Boolean,
                        default: false
                    },
                    changes: [
                        {
                            dose: String,
                            route: {type: String, trim: true},
                            frequency: {type: String, trim: true},
                            reason: String 
                        }
                    ]
                }
        })
    ],
    taken_by: {
        name: String,
        time: Date
    },
    source: {
        patient: Boolean,
        family: Boolean,
        pillpack: Boolean,
        commulity_pharmacy: Boolean,
        family_dr: Boolean,
        med_list: Boolean
    },
    transfer: {
        transfer: Boolean,
        from: String,
        to: String
    },
    last_updated: {
        by: String,
        time: Date
    },
    stopped_medications: [
        new mongoose.Schema({
             name: {type: String, trim: true},
             dose: String,
             route: {type: String, trim: true},
             frequency: {type: String, trim: true},
             last_dose: String,
             reconciliation: {
                     reason: { type: String, enum:["no_discrepancy", "undocumented","unintended"] }
             },
             history: {
                     changes: [
                         {
                             dose: String,
                             route: {type: String, trim: true},
                             frequency: {type: String, trim: true},
                             reason: String 
                         }
                     ]
             }
        })
    ]
});

bmphModel.plugin(findOrCreate);
module.exports = mongoose.model("Bmph", bmphModel); 