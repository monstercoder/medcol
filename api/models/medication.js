var mongoose = require('mongoose');

var medicationSchema = new mongoose.Schema({
    name   : { type: String, trim: true},
    dose  : { type: String, trim: true},
    route    : { type: String, trim: true},
    frequency : { type: String, trim: true},
    last_dose : {type: String, trim: true}
});

module.exports = mongoose.model("medication", medicationSchema);