var mongoose = require('mongoose');
var Bpmh = require('./bmph');


var patientSchema = new mongoose.Schema({
    phin : {type: String, trim: true},
    patient_number : { type: String, trim: true, uppercase: true},
    firstname   : { type: String, trim: true, required : true},
    middlename  : { type: String, trim: true},
    lastname    : { type: String, trim: true, required : true},
    gender      : { type: String, enum: ['M', "F"]},
    birthday    : { type: Date, required: true},
    note: {type: String, trim: true},
    address: {
            line_1: String,
            line_2: String,
            city: String,
            province: String,
            postal: String
    },
    last_updated: {
        by: String,
        time: Date
    },
    bpmh_completed: Boolean
});

patientSchema.pre('save', function(next) {
    this.wasNew = this.isNew;
    next();
});

patientSchema.post('save', function(next) {
    var self = this;
    if (self.wasNew) {
        var bpmh = new Bpmh({
            patient_id: self._id,
            medications: [{}]
        })
        
        bpmh.save(function(err) {
            if (err) {
                console.log("*err*", err)
            }
            self.bpmh = bpmh
            next();
        })
    } else {
        next();  
    }

});

module.exports = mongoose.model("Patient", patientSchema); 