var mongoose = require("mongoose");
var findOrCreate = require('mongoose-findorcreate')

var transferModel = new mongoose.Schema({
    patient_id: { type: mongoose.Schema.ObjectId, ref:'Patient', required: true},
    medications: [
        new mongoose.Schema({
                name: {type: String, trim: true},
                dose: String,
                route: {type: String, trim: true},
                frequency: {type: String, trim: true},
                last_dose: String,
                action: {type: String, default: 'continue', required: true, enum:["continue","discontinue","change"]},
                change_to: {
                    name: {type: String, trim: true},
                    dose: String,
                    route: {type: String, trim: true},
                    frequency: {type: String, trim: true},
                    reason: String
                },
                reconciliation: {
                    reason: { type: String, default: "undocumented", enum:["no_discrepancy", "undocumented","unintended"] },
                    resolved: { type: Boolean, default: false }
                }
        })
    ],
    taken_by: {
        name: String,
        time: Date
    },
    last_updated: {
        by: String,
        time: Date
    },
    from: String,
    to: String
});

module.exports = mongoose.model("Transfer", transferModel);