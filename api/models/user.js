var bcrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
    name : { type: String, trim: true, required: true},
    username : { type: String, trim: true,  required: true, index: {unique: true} },
    password   : { type: String, select:false, required : true}
});

userSchema.pre('save', function(next) {
      
    var user = this;
    if (!user.isModified('password')) return;
    if (!user.role || user.role.length ==0) {
        user.role = [{name: 'clerk'}]
    }

    bcrypt.hash(user.password, null, null, function(err, hash) {
        console.log("generating hash!!!")
        if (err) {
            next(err);
        } else {
            user.password = hash,
            
            next();
        }
    })
});

userSchema.methods.compare = function(password) {
    var user = this;
    return bcrypt.compareSync(password, user.password);
}

module.exports = mongoose.model("User", userSchema);