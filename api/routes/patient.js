var Patient = require('../models/patient');
var Bmph = require('../models/bmph');
var medicationSchema = require('../models/medication')
var DischargePlan = require('../models/dischargePlan')
var Q = require('q');

module.exports = function(express, app) {
    var router = express.Router();
    
    router.post('/patients', function(req, res) {
        var patient = new Patient({
            patient_number : req.body.patient_number,
            firstname : req.body.firstname,
            middlename: req.body.middlename,
            lastname  : req.body.lastname,
            gender    : req.body.gender,
            birthday  : req.body.birthday
        })
        
        patient.save(function(err, patient) {
            if (err) {
                return res.status(500).json({ success: false, message: err});
            } else {
                if (err) {
                     patient.remove(function(){});
                     return res.status(500).json({ success: false, message: error});
                } else {
                     return res.json( {success: true, message: 'saved', patient: patient});
                }
            }
        })
    })
    
    router.get('/patients', function(req, res) {
        var q = req.query.q;

        if (q) {
             Patient
             .find()
             .or({firstname: new RegExp(q, 'i') })
             .or({lastname: new RegExp(q, 'i') })
             .or({middle: new RegExp(q, 'i') })
             .or({patient_number: new RegExp(q, 'i') })
             .exec(
              function(err, patients){
                    if (err) {
                        return res.status(500).json({ success: false, message: err});
                    }
                    return res.json( patients);
            });
        } else {
            Patient.find({}, function(err, patients){
                if (err) {
                    return res.status(500).json({ success: false, message: err});
                }
                return res.json(patients);
            })
        }
    })
    
    router.get('/patient/:patient_id', function(req, res) {
        var patient_id = req.params.patient_id;
        Patient.findById(patient_id, function(err, patient) {
            if (err) {
                return res.status(500).json({ success: false, message: err});
            }
            return res.json(patient);
        })
    })
    
    router.post('/patient/:patient_id', function(req, res) {
        var _id = req.params.patient_id;
         Patient.findByIdAndUpdate(_id, req.body,  { 'new': true }, function(err, p) {
            if (err) {
                console.log("******* error", p)
                return res.status(500).json({ success: false, message: err});
            }
           
            return res.json(p)
        })
    })
    
    router.get('/patient/:patient_id/bpmh', function(req, res) {
        Bmph.findOne({patient_id: req.params.patient_id}, function(err, bp) {
            if (err) {
                return res.status(500).json({ success: false, message: err});
            }

            return res.json(bp);
        })
    })
    
    router.post('/patient/:patient_id/bpmh', function(req, res) {
        Bmph.findOrCreate({patient_id: req.body.patient_id}, {
                patient_id: req.body.patient_id
            }, function(err, bmph){
                bmph.patient_id = req.body.patient_id
                bmph.allergy= req.body.allergy
                bmph.medications=  req.body.medications
                var update_patient = (bmph.completed != req.body.completed)
                bmph.completed = req.body.completed
                if (bmph.medications) {
                    bmph.medications.forEach(function(m) {
                        m.history.admission_prescription = true;
                    })
                };
                
                bmph.source = req.body.source
                bmph.taken_by = req.body.taken_by

                bmph.save(function(err) {
                    if (err) {
                        return res.status(500).json({ success: false, message: err});
                    } else {
                        
                        if (update_patient) {
                            Patient.findByIdAndUpdate(req.body.patient_id, {bpmh_completed: bmph.completed}, {'new': true}, function(err, p) {
                                console.log("***", p.bpmh_completed);
                            })
                        }
                        return res.json( {success: true, message: 'saved', bmph: bmph});
                    }
                });
        });
    });
      
    return router;
}

