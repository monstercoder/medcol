var Bpmh = require('../models/bmph');
var PhysicianOrders = require('../models/physicianOrders')
var Q = require('q');

module.exports = function(express, app) {
    var router = express.Router();
    router.get('/:patient_id/discharge', function(req, res){
           var bpmh_task = Bpmh.findOne({patient_id: req.params.patient_id});
           var physician_orders_task = PhysicianOrders.find({patient_id: req.params.patient_id})
           Q.all([bpmh_task, physician_orders_task])
           .fail(function(err) {
                   console.log("&&&&",err)
                   return res.status(500).json({ success: false, message: error});
           })
           .done(function(values) {
                   var bpmh = values[0] || {medications:[]};
                   var orders = values[1] || {};

                   var new_medications = [];
                   var continued = [];
                   var changed = [];
                   var stopped = [];
                   var reconciled = true;
                   
                   function classify(med) {
                       if (med.history && med.history.admission_prescription == true) {
                           if (med.action == 'continue') {
                               med.reconciliation.resolved = true;
                               if (med.history.changes && med.history.changes.length >0) {
                                   changed.push(med);      
                               } else {
                                   continued.push(med);
                               }
                               

                           } else if (med.action == 'change') {
                               if (!med.history.changes || med.history.changes.length == 0) {
                                   med.history.changes = [{
                                       
                                           name: med.name,
                                           dose: med.dose,
                                           route: med.route,
                                           frequency: med.frequency
                                       
                                   }]     
                               }
                               med.dose = med.change_to.dose;
                               med.frequency = med.change_to.frequency;
                               med.route = med.change_to.route;
                               reconciled = reconciled && med.reconciliation.resolved;
                              
                               changed.push(med);
                           } else if (med.action== 'discontinue') {
                               stopped.push(med)
                               reconciled = reconciled && med.reconciliation.resolved;;
                           }
                       } else {
                           if (med.action == 'continue') {
                               med.reconciliation.resolved = true;
                               new_medications.push(med)

                           } else if (med.action == 'change') {
                               med.dose = med.change_to.dose;
                               med.frequency = med.change_to.frequency;
                               med.route = med.change_to.route;
                               reconciled = reconciled && med.reconciliation.resolved;
                               new_medications.push(med);
                           } else {
                               reconciled = reconciled && med.reconciliation.resolved;
                           }
                       }
                   }

                   bpmh.medications.map(classify);
                   if (bpmh.stopped_medications ) {
                      bpmh.stopped_medications.forEach(function(m) {
                          stopped.push(m);
                      }) 
                   };
                   
                   orders.forEach(function(o) {
                       if (o.medications) {
                           o.medications.map(classify)
                       }
                   });

                   return res.json( { allergy: bpmh.allergy, new_medications: new_medications, continued: continued, changed: changed, stopped: stopped, reconciled : reconciled });
            });
    })
    return router;
}