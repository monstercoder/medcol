var Bpmh = require('../models/bmph');
var PhysicianOrders = require('../models/physicianOrders')
var Q = require('q');

module.exports = function(express, app) {
    var router = express.Router();
    router.post('/:patient_id/physicion_order', function(req, res){
        var new_order = new PhysicianOrders(req.body);
        new_order.patient_id = req.params.patient_id;
        new_order
        .save(function(err, o) {
            if (err) {
                console.log("$$$$", err)
                return res.status(500).json({ success: false, message: err});
            }

            return res.json(o);
        })
    });
    
    router.put('/:patient_id/physicion_order/:id', function(req, res){
        req.body.patient_id = req.params.patient_id;
        console.log("&&&&&&&&", req.body.data)
        PhysicianOrders.findByIdAndUpdate(req.params.id, req.body.data, {'new': true}, function(err, o) {
            if (err) {
                console.log("$$$$", err)
                return res.status(500).json({ success: false, message: err});
            }

            return res.json(o);
        })
    });
    
     router.delete('/:patient_id/physicion_order/:id', function(req, res){
         PhysicianOrders.remove({_id: req.params.id}, function(err) {
             if (err) {
                 console.log("$$$$", err)
                 return res.status(500).json({ success: false, message: err});
             }
             return res.json({success: true});
         })
          
     });

    return router
};