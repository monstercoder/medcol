var webtoken = require('jsonwebtoken');
var User = require('../models/user');
var config = require('../config/config')
function createToken(user) {
    return webtoken.sign({
        id: user._id,
        username: user.username,
        name: user.name,
        role: user.role
    }, config.secretKey)
};

module.exports = function(express, app) {
    var router = express.Router();
    router.post('/login', function(req, res){
        var username = req.body.username;
        User.findOne({username : username})
            .select('username password role')
            .exec(function(err, user) {
                if (err) {
                    res.status(500).json({success: false, message: err});
                } else {
                   
                    if (!user)
                        res.status(403).json({success: false, message: "User doesn't exist!"})
                    else if (user.compare(req.body.password) === true) {
                        var token = createToken(user);
                        res.json({ success: true, message: "login successfully.", token: token, roles: user.role , user: user.username});
                    } else {
                       res.status(403).json({success: false, message: "Username & password doesnt match!"})
                    }
                }
            })
    });

    return router;
}