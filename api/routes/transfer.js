var Bpmh = require('../models/bmph');
var PhysicianOrders = require('../models/physicianOrders')
var Transfer = require('../models/transfer')
var Q = require('q');

module.exports = function(express, app) {
    var router = express.Router();
    
    router.get('/:patient_id/transfer', function(req, res){
        var bpmh_task = Bpmh.findOne({patient_id: req.params.patient_id});
        var physician_orders_task = PhysicianOrders.find({patient_id: req.params.patient_id})
        Q.all([bpmh_task, physician_orders_task])
        .fail(function(err) {
                console.log("&&&&",err)
                return res.status(500).json({ success: false, message: error});
        })
        .done(function(values) {
                var bpmh = values[0] || {medications:[]};
                var orders = values[1] || {};
                
                var new_medications = [];
                var continued = [];
                var changed = [];
                var stopped = [];
                var reconciled = true;
                function classify(med) {
                    if (med.action == 'continue') {
                        med.reconciliation.resolved = true;
                        continued.push(med)
  
                    } else if (med.action == 'change') {
                        med.dose = med.change_to.dose;
                        med.frequency = med.change_to.frequency;
                        med.route = med.change_to.route;
                        reconciled = reconciled && med.reconciliation.resolved;
                        changed.push(med);
                    } else if (med.action== 'discontinue') {
                        stopped.push(med)
                        reconciled = reconciled && med.reconciliation.resolved;;
                    }
                }
                
                bpmh.medications.map(classify);
                orders.forEach(function(o) {
                    if (o.medications) {
                        o.medications.map(classify)
                    }
                    
                });
                
                
                return res.json( { continued: continued, changed: changed, stopped: stopped, reconciled : reconciled });
        })
        
    });
    
    
    router.post('/:patient_id/transfer', function(req, res){
         var bpmh_task = Bpmh.findOne({patient_id: req.params.patient_id});
            var physician_orders_task = PhysicianOrders.find({patient_id: req.params.patient_id})
            Q.all([bpmh_task, physician_orders_task])
            .fail(function(err) {
                    console.log("&&&&",err)
                    return res.status(500).json({ success: false, message: error});
            })
            .done(function(values) {
                    var bpmh = values[0];
                    var orders = values[1];
                    var medications = [];
                    var discontinued = [];
                    bpmh.medications.map(cleanUp);
                    orders.forEach(function(o) {
                        if (o.medcations ) {
                            o.medications.map(cleanUp)
                        }
                        
                    });
                    
                    bpmh.medications = medications;
                    bpmh.stopped_medications = discontinued;
                    bpmh.transfer = { 
                        transfered: true,
                        from: req.query['from'],
                        to: req.query['to']
                    };
                    bpmh.save(function(err, saved) {
                        
                        if (err) {
                             console.log("&&&&",err)
                            return res.status(500).json({ success: false, message: error});
                        }
                        PhysicianOrders.remove({patient_id: req.params.patient_id},function() {
                            
                        });
                        return res.json( { medications: saved.medications });
                    });
                    
                    function cleanUp(med) {


                         if (med.action == 'continue') {
                             medications.push(med);

                         } else if (med.action == 'change') {
                             if (med.history.admission_prescription == true) {
                                 med.history.changes.push( {
                                     dose: med.dose,
                                     route: med.route,
                                     frequency: med.frequency
                                 });
                             }
                             
                             med.dose = med.change_to.dose;
                             med.frequency = med.change_to.frequency;
                             med.route = med.change_to.route;
                             med.last_dose ='';
                           

                             med.change_to.dose = '';
                             med.change_to.frequency = '';
                             med.change_to.route = '';

                             medications.push(med);

                         } else if (med.action='discontinue' && med.history && med.history.admission_prescription == true) {
                             discontinued.push(med);
                         };
                         med.reconciliation.reason = "undocumented";
                         med.reconciliation.resolved = false;
                         med.action = 'continue';
                     }
            })
            
 
            
            
            
    })
    
    return router;
}
