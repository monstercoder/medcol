var User = require('../models/user');

module.exports = function(express, app) {
    var router = express.Router();
    router.post('/users', function(req, res){
        var user = new User({
            name: req.body.name,
            username: req.body.username,
            password: req.body.password,
            role: req.body.role
        });
      
        user.save(function(err, u){
            if (err) {
                return res.status(500).json({success: false, message: err});
            } else {
                return res.json({success: true, user: u});
            }    
        })
    });


    return router;
}