var Bpmh = require('../models/bmph');
var PhysicianOrders = require('../models/physicianOrders')
var Q = require('q');

module.exports = function(express, app) {
    var router = express.Router();
    
    router.get('/:patient_id/current_prescription', function(req, res){
        
        var bpmh_task = Bpmh.findOne({patient_id: req.params.patient_id});
        var physician_orders_task = PhysicianOrders.find({patient_id: req.params.patient_id})
        Q.all([bpmh_task, physician_orders_task])
        .fail(function(err) {
                console.log("&&&&",err)
                return res.status(500).json({ success: false, message: error});
        })
        .done(function(values) {
                var bpmh = values[0] || {};
                orders = values[1] || {};
                return res.json( { bpmh_id: bpmh._id, reconciled_medications: bpmh.medications, allergy: bpmh.allergy, physician_orders: orders});
        })
    });
    
    
    return router;
}

