var Patient = require('../models/patient');
var Bpmh = require('../models/bmph');

var Q = require('q');

module.exports = function(express, app) {
    var router = express.Router();
    
    router.post('/:bpmh_id/medication/:id', function(req, res){
        Bpmh
        .findById(req.params.bpmh_id)
        .exec(function(err, b) {
            if (err) {
                  return res.status(500).json({ success: false, message: error});
            }

            var med = b.medications.filter(function(m) {
                   return  m._id == req.params.id;
            })[0]
            //console.log('****', b.medications[0].id == req.params.id)
            //console.log("****", med ,req.body)
            med.action = req.body.data.action;
            med.change_to = req.body.data.change_to;
           
            b.save(function(err, saved) {
                return res.json(med)
            })
        })

       
    });
    
    return router;
}