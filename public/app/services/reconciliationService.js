angular.module('app.services.reconciliationService',[])
.factory('reconciliationService', function($resource) {
     return $resource('/api/patient/:bpmh_id/reconciliation/:id', {bpmh_id: '@bpmh_id', id: '@_id'})
})