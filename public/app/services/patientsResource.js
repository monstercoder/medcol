angular.module('app.services.patientsService',['ngResource'])
.factory('patients', ['$resource', function($resource) {
    return $resource('/api/patients', {})

}])