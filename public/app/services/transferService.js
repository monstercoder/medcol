angular.module('app.services.transferService',[])
.factory('transferService', function($resource) {
     return $resource('/api/patient/:patient_id/transfer/:id')
})