angular.module("app.services.authService", [])
.factory('auth', ['$http', '$q', function($http, $q) {
    var authService = {}
    authService.login = function(loginData) {
        var deferred = $q.defer()
        authService.logout();
        return $http.post('/api/login', loginData)
        .success(function(result) {
            deferred.resolve();
            return result;
        })
        .error(function(err) {
            deferred.reject(err);
        })
        
        return deferred.promise();
    };
    
    authService.isLoggedIn = function(next) {
        return window.localStorage['token'];
    }
    
    authService.logout = function() {
        window.localStorage.removeItem('token');
    }
    return authService;
}])