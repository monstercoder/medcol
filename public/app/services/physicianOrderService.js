angular.module('app.services.physicianOrderService',[])
.factory('physicianOrderService', function($resource) {
    return $resource('/api/patient/:patient_id/physicion_order/:id', 
        {
            patient_id: '@patient_id', id: '@_id'
        },
        {
            update: { method:'PUT' }
        })
})