angular.module('app.services.currentPrescriptionService',[])
.factory('currentPrescriptionService', ['$resource', function($resource) {
    return $resource('/api/patient/:patient_id/current_prescription', {patient_id: '@patient_id'})
}])
.factory('medicationService', ['$resource',function($resource){
    return $resource('/api/patient/:bpmh_id/medication/:id', {bpmh_id: '@bpmh_id', id: '@_id'})
}])