angular.module('app.services.dischargePlanService',[])
.factory('dischargePlanService', function($resource) {
    return $resource('/api/patient/:patient_id/discharge', {patient_id: '@patient_id'})
})