angular.module("app.services", ['app.services.authService', 
    'app.services.patientService',
    'app.services.bpmhService',
    'app.services.dischargePlanService',
    'app.services.currentPrescriptionService',
    'app.services.physicianOrderService',
    'app.services.reconciliationService',
    'app.services.transferService']);