angular.module('app.services.patientService',[])
.factory('patient', function($resource) {
    return $resource('/api/patient/:patient_id', {})
})
