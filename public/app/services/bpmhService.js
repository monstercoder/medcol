angular.module('app.services.bpmhService',[])
.factory('bpmhService', function($resource) {
    return $resource('/api/patient/:patient_id/bpmh', {patient_id: '@patient_id'})
})
