angular.module("medcol", ['ngMaterial', 'ui.router', 'app.services', 'app.controllers'])
    .config(['$stateProvider', '$urlRouterProvider','$mdThemingProvider', function($stateProvider, $urlRouterProvider, $mdThemingProvider) {
            $urlRouterProvider
                .when('', 'home' )
                .when('login', 'login' )
                .when('current_prescription', 'current_prescription')
                .when('reconciliation', 'reconciliation')
                .when('transfer','transfer')
                .when('bpmh', 'bpmh')
                .when('discharge', 'discharge')
                .otherwise('');

            $stateProvider
                .state('login', {
                    url: "/login",
                    templateUrl: "/app/views/login.html",
                    controller: 'loginController'
                })
                .state('home', {
                    url:  "/home",
                    templateUrl: '/app/views/Main.html',
                    controller: 'mainController',
                    access: {
                          requireLogin: true
                    }
                })
                .state('dashboard',
                 {
                    url: '/patient/:id',
                    templateUrl: '/app/views/patient/dashboard.html',
                    controller: 'dashboardController',
                    resolve: {
                        patient: ['$stateParams', 'patient', function($stateParams, patient) {
                          return patient.get({patient_id: $stateParams.id}).$promise;
                        } ],
                        patient_id: ['$stateParams', function($stateParams) {
                            return $stateParams.id;
                        }],
                        medications_lookup: ['$http', '$q', function($http, $q) {
                             var deferred = $q.defer();
                             $http.get('medications.json')
                             .success(function(data) {
                                 deferred.resolve(data);
                             })    
                             .error(function (msg) {
                                         deferred.reject(msg);
                            });

                            return deferred.promise;
                                              
                        }]
                        
                    },
                    access: {
                            requireLogin: true
                    }
                })
                .state('dashboard.profile',
                 {
                    url: '/profile',
                    templateUrl: '/app/views/patient/patient.profile.html',
                    controller: 'profileController',
                    access: {
                            requireLogin: true
                    }
                })
                .state('dashboard.bpmh',
                   {
                      url: '/bpmh',
                      templateUrl: '/app/views/patient/patient.bpmh.html',
                      controller: 'bpmhController',
                      resolve: {
                        bpmhData: ['patient_id','bpmhService', function(patient_id, bpmService){
                            
                            return bpmService.get({patient_id: patient_id}).$promise
                        }]
                      },
                      access: {
                              requireLogin: true
                      }
                  })
                  .state('dashboard.current_prescription',
                     {
                        url: '/current_prescription',
                        templateUrl: '/app/views/patient/patient.current_prescription.html',
                        controller: 'currentPrescriptionController',
                        resolve: {
                            current_prescription_data: ['currentPrescriptionService','patient_id', function(currentPrescriptionService, patient_id) {
                                return currentPrescriptionService.get({ patient_id: patient_id }).$promise
                            }],
                        },
                        access: {
                                requireLogin: true
                        }
                    })
                    .state('dashboard.reconciliation',
                    {
                        url: '/reconciliation',
                        templateUrl: '/app/views/patient/reconciliation.html',
                        controller: 'reconciliationController',
                        resolve: {
                            current_prescription_data: ['currentPrescriptionService','patient_id', function(currentPrescriptionService, patient_id) {
                                return currentPrescriptionService.get({ patient_id: patient_id}).$promise
                            }],
                        },
                        access: {
                                requireLogin: true
                        }
                    })
                    .state('dashboard.transfer',
                    {
                        url: '/transfer',
                        templateUrl: '/app/views/patient/transfer.html',
                        controller: 'transferController',
                        resolve: {
                            transfer_data: ['transferService', 'patient_id',function(transferService, patient_id) {
                                return transferService.get({patient_id: patient_id}).$promise
                            }]
                        }, 
                        access: {
                                requireLogin: true
                        }
                    })
                    .state('dashboard.discharge',
                       {
                          url: '/discharge',
                          templateUrl: '/app/views/patient/patient.discharge.html',
                          controller: 'dischargeController',
                          resolve: {
                              discharge_plan: ['dischargePlanService', 'patient_id',function(dischargePlanService, patient_id) {
                                  return dischargePlanService.get({patient_id: patient_id}).$promise;
                              }]
                          },
                          access: {
                                  requireLogin: true
                          }
                      });
                  // Configure a dark theme with primary foreground yellow

                  $mdThemingProvider.theme('default')
                    .primaryPalette('blue-grey')
                    .accentPalette('red');
    
    }])
    .run(['$rootScope', '$state','$timeout', 'auth', function($rootScope, $state, $timeout, auth) {
        $rootScope.$on('$stateChangeStart', function(event, next) {
            if (next.access && next.access.requireLogin == true && !auth.isLoggedIn(next)) {
                console.log("transition to login");
                event.preventDefault()
                $state.go("login",{});
            }
            
            angular.element(document.querySelector('#drawer')).removeClass('is-visible')
        });
        $rootScope.$on('$viewContentLoaded', function(event, next) {
          $timeout( function()  {
            componentHandler.upgradeAllRegistered();
          })
        });
    }])
