angular.module('app.controllers.mainController', ['ngResource'])
.controller('mainController', ['$scope', '$state',  '$resource',function($scope, $state,  $resource) {
    var patientsResource = $resource('/api/patients');
    $scope.patients =  patientsResource.query();
    $scope.onPatientSelected = function(patient) {
        $state.go("dashboard", { id: patient._id} );
    }
    $scope.signOut = function() {
         window.localStorage['token'] = null;
         //console.log("sign out")
         $state.go("login");
    }
}])