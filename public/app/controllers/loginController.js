angular.module('app.controllers.loginController', ['app.services.authService'])
.controller('loginController', ['$scope', '$state','auth', function($scope, $state, auth) {
        window.localStorage.removeItem('token');
        
        $scope.reset = function() {
          $scope.message="";
        };
        
        $scope.submit = function () {
            auth
            .login({ username: $scope.username, password: $scope.password })
            .success(function(result) {
                window.localStorage['token'] = result.token;
                //window.localStorage['roles'] = result.roles;
                
                window.localStorage['user'] = result.user;
                window.localStorage['premissions'] = resovlePermissions(result.roles || []);
                $state.go("home",{},{location: 'replace'});
            })
            .error(function(err) {
                $scope.message = err.message
            });
        };
}])

function resovlePermissions(roles) {
    var result = {
        profile: true,
        bpmh: {},
        transfer: false
    }
    
    for(var i =0 ; i < roles.length; i++) {
        var name = roles[i];
        switch(name) {
            case 'admission':
                result.profile = true;
                break;
            case 'clerk':
                result.profile = true;
                result.bpmh.view = true;
                result.bpmh.change = true;
                result.transfer = true;
                break;
            case 'nurse':
                result.bpmh.view = true;
                result.bpmh.change = true;
                result.transfer = true;
                break;
            case 'doctor':
                result.bpmh.view = true;
                result.bpmh.change = true;
                result.medication = true;
                break;
            case 'pharmacist':
                result.bpmh.view = true;
                result.bpmh.reconciliation = true;
                break;
            case 'discharge_educator':
                result.bpmh.view = true;
                result.bpmh.reconciliation = true;
                break;
            default:
                result.profile = true;
        }
        
    }
}