angular.module('app.controllers.patient.bpmhController', [])
.controller('bpmhController', ['$rootScope', '$scope', '$timeout','$filter', '$log','bpmhData','bpmhService','medications_lookup', 
    function($rootScope, $scope, $timeout, $filter, $log, bpmhData, bpmhService, medications_lookup) {
        $scope.completed = bpmhData.completed
        $scope.medications=  bpmhData.medications || [] 
        $scope.allergy = bpmhData.allergy || {hasAllergy: 'No', content: ""}
        $rootScope.title = "Best Possible Medication History ";
        $rootScope.isSearchAble = true;
        $rootScope.show_demographics = true;
        $scope.source = bpmhData.source
        $scope.taken_by = bpmhData.taken_by || {}
        $scope.permissions = window.localStorage['premissions'];
                                                                          console.log("***", medications_lookup.length)
        $scope.addMedicationRecord = function() {
            
            $scope.medications.push({
                    name: "",
                    action: 'continue',
                    type: "prescription",
                    change_to: {
                        name: "",
                        type: ""
                    }
            });
        };
        
        $scope.removeMedicationRecord = function() {
            //console.log(1, $scope.reconciliation.medications);
            $scope.medications= $filter('filter')($scope.medications, function(a) {
                return !a.selected;
            });
            
        }
        
        $scope.$watch('medications', function(newvalue, oldvalue) {
            //$scope.order_by =undefined;
            if (newvalue == oldvalue) {
               return;
                
            }
            $scope.taken_by.name = window.localStorage['user']
            $scope.taken_by.time = new Date()
            updateBpmh();
        }, true);
        
        $scope.$watch('source', function(newvalue, oldvalue) {
            //$scope.order_by =undefined;
            if (newvalue == oldvalue) {
               return;
                
            }
            $scope.taken_by.name = window.localStorage['user']
            $scope.taken_by.time = new Date()
            updateBpmh();
        }, true);
        
        
        $scope.$watch('completed', function(newvalue, oldvalue) {
            //$scope.order_by =undefined;
            if (newvalue == oldvalue) {
               return;
                
            }

            updateBpmh();
        }, true);
        
        
        $scope.$watch('allergy', function(newvalue, oldvalue) {
            if (newvalue == oldvalue) {
               return;
                
            }
            $scope.taken_by.name = window.localStorage['user']
            $scope.taken_by.time = new Date()
            
            updateBpmh();
        }, true);
        
        var timeout = null;
        
        function updateBpmh() {
            if (timeout) {
                $timeout.cancel(timeout);
            }
            timeout = $timeout(function() {
                //console.log("update bpmh", $scope.medications);
                 var bpmh = new bpmhService({
                     patient_id: $scope.patient._id, 
                     allergy: $scope.allergy, 
                     source: $scope.source,
                     medications: $scope.medications,
                     taken_by: $scope.taken_by,
                     completed: $scope.completed
                     });
                 bpmh.patient_id = $scope.patient._id;
                 bpmh.$save(function(b, res) {
                    

                    //window.user = window.localStorage['user'];
                 });
            }, 500);
        
        }
        
        var self = this;

        
        $scope.querySearch   = querySearch;
        $scope.selectedItemChange = selectedItemChange;

        function querySearch (query) {
            console.log(medications_lookup)
            if (!query || query.length <3) {
                return []
            }
            return [medications_lookup[0],medications_lookup[1]];
        }

        function searchTextChange(text, medication_rec) {
              medication_rec.name = item.brand_name;
              //$log.info('Text changed to ' + text);
        }

        function selectedItemChange(item, medication_rec) {
            medication_rec.name = item.brand_name;
            //$log.info('Item changed to ' + JSON.stringify(item));
        }

    }
])
