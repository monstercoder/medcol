angular.module('app.controllers.patient.dischargeController', [])
.controller('dischargeController', ['$rootScope', '$scope','discharge_plan', function($rootScope, $scope, discharge_plan) {
    $rootScope.title = "Discharge Medication Plan and Prescription";
    $scope.data = discharge_plan;
    console.log(discharge_plan);
}])