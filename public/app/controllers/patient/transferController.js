angular.module('app.controllers.patient.transferController', [])
.controller('transferController', ['$scope','$timeout', '$mdDialog','transferService','transfer_data', 'patient_id' ,
    function($scope, $timeout, $mdDialog, transferService, transfer_data, patient_id) {
        console.log(transfer_data);
        $scope.data = transfer_data;
        $scope.showConfirm = function(ev) {
            
            var confirm = $mdDialog.confirm()
                  .title('Would you like to start tranfer?')
                  .content('All medications will be shown as current after transfer.')
                  .ariaLabel('Transfer ')
                  .ok('Yes')
                  .cancel('Cancel')
                  .targetEvent(ev);

            $mdDialog.show(confirm).then(function() {
              console.log("yes")
              var t = new transferService({
                  destination: $scope.data.destination
              })
              t.$save({ patient_id: patient_id}, function(err, result) {
                  console.log("$$$$$$", err, result);
                  
                  alert("Transfer succeed!")
                  $scope.data.continued = result.medications;
                  $scope.data.stopped = null;
                  $scope.data.changed = null;
              })
            }, function() {
                
            });
          };
    }])