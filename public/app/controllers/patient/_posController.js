angular.module("app.controllers.patient.posController",[])
.controller("posController",['$scope', '$rootScope', '$timeout','patient_id', 'physician_orders','posService', function($scope, $rootScope, $timeout, patient_id, physician_orders, posService) {
    console.log("physician orders", physician_orders)
    $scope.physician_orders = physician_orders || {};
    $rootScope.title = "Physician Order Sheet";
    $rootScope.isSearchAble = true;
    var timeout = null;
     
    $scope.newOrder = function() {
        $scope.physician_orders.orders.push({
            order_time: Date.now(),
            patient_id: patient_id,
            medications: [{ type: 'prescription' },{type: 'prescription'},{type: 'prescription'},{type: 'prescription'},{type: 'prescription'}],
            general_orders:[{type: 'non-prescription'},{type: 'non-prescription'},{type: 'non-prescription'},{type: 'non-prescription'},{type: 'non-prescription'}]
        })
    }
    function updateOrder(data)  {
        
        if (timeout) {
            $timeout.cancel(timeout);
        }
        timeout = $timeout(function() {
            var pos = new posService(data);
            pos.patient_id = patient_id;
            pos.$save(function(p){
                console.log("pos saved!")
            })
        }, 1000);

    }

    $scope.$watch('physician_orders', function(newOrder, oldOrder) {
        
        if (newOrder == oldOrder) {
            return;
        }

        updateOrder(newOrder);
    }, true);
   
    
}])
.controller("physicianOrderController", ['$scope','$timeout','posService', function($scope, $timeout, posService) {
    var timeout = null;
       function updateOrder(order)  {
           if (timeout) {
                $timeout.cancel(timeout);
            }
            timeout = $timeout(function() {
                 console.log("update order", order);
                 /*var orders = new posService(physician_orders);
                 orders.$save(function(b, res) {
                     console.log("order saved", b, res);

                 });*/
            }, 500);

       }

       $scope.$watch('order', function(newOrder, oldOrder) {
           console.log("orders changed");
           if (newOrder == oldOrder) {
               return;
           }

           updateOrder(newOrder);
       }, true);

    
}])