angular.module('app.controllers.patient.currentPrescriptionController', [])
.controller('currentPrescriptionController', ['$scope','$timeout', '$mdDialog','medicationService','physicianOrderService','current_prescription_data','patient_id' ,
    function($scope, $timeout, $mdDialog,medicationService, physicianOrderService,current_prescription_data, patient_id) {
        console.log(current_prescription_data)
        $scope.data = current_prescription_data;
        var bpmh_id = current_prescription_data.bpmh_id;
        var physician_orders = $scope.data.physician_orders;
        /*
        $scope.$watch('data.reconciled_medications', function(oldvalue, newvalue) {
            console.log(oldvalue, newvalue);
        }, true)*/
        
        $scope.onMedicationChanged = function(med_rec) {
            update_econciled_medication(med_rec);
            
        }
        
        $scope.onPrescriptionChanged = function(physician_order, med_rec) {
            update_prescription(physician_order);
        };
        
        $scope.showAdvanced = function(ev) {
            $mdDialog.show({
              controller: DialogController,
              templateUrl: 'dialog1.tmpl.html',
              targetEvent: ev,
              clickOutsideToClose: false
            })
            .then(function(answer) {
              $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.status = 'You cancelled the dialog.';
            });
          };
          
        var timeout= null;
        function update_econciled_medication(data) {
            if (data._id) {
                if (timeout) {
                    $timeout.cancel(timeout);
                }
                timeout = $timeout(function() {
                    data.bpmh_id =  bpmh_id;
                    var u = new medicationService({data})
                    
                    u.$save({bpmh_id: bpmh_id, id: data._id});
                }, 500);
            } else {
                console.error("new medication can not be updated")
            }
        }
        
        var timeout2= null;
        function update_prescription(data) {
            console.log("!todo:  only update the effected medication")
            if (data._id) {
                if (timeout2) {
                    $timeout.cancel(timeout2);
                }
                timeout2 = $timeout(function() {
                    var u = new physicianOrderService({data})
                    
                    physicianOrderService.update({patient_id: patient_id, id: data._id},u);
                }, 500);
            } else {
                console.error(" physician order can not be updated")
            }
        }
        
        function DialogController($scope, $mdDialog) {
          $scope.data= new medicationService({
              medications: [{},{},{},{},{}],
              prscriber: {
                  name: window.localStorage['user']
              }
          });
          
          $scope.hide = function() {
            $mdDialog.hide();
          };

          $scope.cancel = function() {
            $mdDialog.cancel();
            $scope.data= new medicationService({
                medications: [{},{},{},{},{}]
            });
          };

          $scope.save = function() {
            $mdDialog.hide();
            var medications = []
            $scope.data.medications.forEach(function(m) {
                if (m.name )  {
                    medications.push(m);
                }
            })
            
            console.log(medications)
            
            var order = new physicianOrderService({
                medications: medications,
                prescriber: $scope.data.prescriber
            })
            
            order.$save({patient_id: patient_id});
            physician_orders.push(order);
            medications = [];
            $scope.data= new medicationService({
                medications: [{},{},{},{},{}]
            });
          };
        }
    }
])