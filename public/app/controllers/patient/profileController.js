angular.module('app.controllers.patient.profileController', [])
.controller('profileController', ['$rootScope', '$scope', '$timeout','patient',function($rootScope, $scope,$timeout, patient) {
    $scope.content ="Profile";
    $rootScope.title = "Patient Profile";
    $rootScope.isSearchAble = false;
    $rootScope.show_demographics = false;
    var updating = false;
 
    $scope.$watch('patient', function(newvalue, oldvalue) {
        if (newvalue == oldvalue) {
           
           return;
            
        }
        update_profile();
        updating = false;
    }, true)
    
    var timeout = null;
    
    function update_profile() {
        if (timeout) {
            $timeout.cancel(timeout);
        }
        timeout = $timeout(function() {
            $scope.patient.$save({patient_id: $scope.patient._id})
          
        }, 500);
 
    }
}])