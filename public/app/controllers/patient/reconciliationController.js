angular.module('app.controllers.patient.reconciliationController', [])
.controller('reconciliationController',['$scope','reconciliationService','physicianOrderService','$timeout', '$mdDialog','current_prescription_data','patient_id' ,
    function($scope,ReconciliationService, physicianOrderService, $timeout, $mdDialog, current_prescription_data, patient_id) {
        console.log(current_prescription_data)
        $scope.data = current_prescription_data;
        var bpmh_id = current_prescription_data.bpmh_id;
        
        var timeout= null;
        var timeout2= null;
        $scope.onReconciled = function(medication_rec) {
            if (medication_rec.reconciliation.reason == 'no_discrepancy') {
                medication_rec.reconciliation.resolved = true;
            }

            if (timeout) {
                    $timeout.cancel(timeout);
            };
            timeout = $timeout(function() {
                    var recon = new ReconciliationService(medication_rec.reconciliation);
                    recon.$save({bpmh_id: bpmh_id, id: medication_rec._id});
            }, 500);
        }
        
        $scope.onPrescriptionReconciled = function(data, medication_rec) {
            console.log("!todo:  only update the effected medication reconciliation")
            if (data._id) {
                if (timeout2) {
                    $timeout.cancel(timeout2);
                }
                timeout2 = $timeout(function() {
                    var u = new physicianOrderService({data})
                    
                    physicianOrderService.update({patient_id: patient_id, id: data._id},u);
                }, 500);
            } else {
                console.error(" physician order can not be reconciliated")
            }
        }
        
        $scope.changed = function(med) {
            return med.action != 'continue'
        }
}])
.filter('action_map', function() {
    var map = {
        contine: 'Continue',
        discontinue: 'Do not order',
        change: 'Change To'
    }
  return function(action) {
    return map[action]
  };
});